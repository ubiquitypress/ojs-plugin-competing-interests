{**
 * plugins/generic/competingInterests/competingInterests.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Edit CompetingInterests competingInterests 
 *
 *}
 
<link rel="stylesheet" href="{$baseUrl}/plugins/generic/competingInterests/css/competingInterests.css">
<script src="{$baseUrl}/plugins/generic/competingInterests/js/CompetingInterestsHandler.js"></script>

<style>
.popup-overlay {
	/*Hides pop-up when there is no "active" class*/
	visibility: collapse;
}
</style>

<div id="modals-container">
	<div aria-expanded="true" data-modal="ensureAnonymousReview" class="v--modal-overlay scrollable popup-overlay">
		<div class="v--modal-background-click">
			<div class="v--modal-top-right"></div> 
				<div role="dialog" aria-modal="true" class="v--modal-box v--modal v--modal-dialog" style="top: 0px; left: 164px; width: 600px; height: auto;">
					<div class="modal modal--dialog">
						<span tabindex="0"></span>
						<div>
							<div class="modal__header">
								<h2 class="modal__title">{translate key="manager.setup.competingInterestsPolicy"}</h2>
								<button class="modal__closeButton">
									<span aria-hidden="true" class="close">×</span><span class="-screenReader">Close</span>
								</button>
							</div>
							<div class="modal__content popup-content">
								<div>{$competingInterestsPolicy}</div>
								<div class="modal__footer">
									<button class="pkpButton close"> {translate key="common.ok"} </button>
								</div>
							</div>
						</div>
					<span tabindex="0"></span>
				</div> 
			</div>
		</div>
	</div>
</div>

{fbvFormSection title="manager.submissions.competingInterests.title" for="competingInterests" required=$competingIrequired}
	<p class="pkp_help">{translate key="manager.submissions.competingInterests.description"}</p>
	{fbvElement type="textarea" multilingual=true name="competingInterests" id="competingInterests" value=$competingInterests rich="extended" }
{/fbvFormSection}