/**
 * @defgroup plugins_generic_competingInterests_js
 */
/**
 * @file js/CompetingInterestsHandler.js
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2000-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file LICENSE.
 *
 * @class CompetingInterestsHandler.js
 * @ingroup plugins_generic_competingInterests_js
 *
 * @brief Handle the competingInterests form.
 */

(function($) {
	$("#cipolicy-btn").on("click", function(e) {
		console.log('test');
		$(".popup-overlay, .popup-content").addClass("active");
	});

	$(".close, .popup-overlay").on("click", function(e) {
		$(".popup-overlay, .popup-content").removeClass("active");
		e.preventDefault();
		e.stopPropagation();
	});
}(jQuery));